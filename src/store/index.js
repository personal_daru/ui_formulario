import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VuexPersist from 'vuex-persist'
import { minix, pregunta } from '../components/functions/alertas'

Vue.use(Vuex)

const vuexPersist = new VuexPersist({
  key: 'rhomio',
  storage: window.localStorage,
  reducer: state => ({

  }) 
})


export default new Vuex.Store({
  state: {
    preferencias: {
      IP: process.env.NODE_ENV == 'production' ? '' : 'http://localhost:1337' ,
      PUERTO: 8005
    },
  },
  getters: {
  },
  mutations: {
  },
  actions: {
    async guardar_data({commit, state}, data){
      try {
        const config = {
          method: 'post',
          url: `${state.preferencias.IP}/api/${data.api}`,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${state.token_sesion}`
          },
          data: { data: data.formulario }
        }

        const r = await axios(config)

        if (r.status == 200) {
          if(r.data.message){
            minix({icon: 'info', mensaje: r.data.message, tiempo: 6000})
          }else{
            if (r.data.mensaje) {
              
              minix({icon: 'success', mensaje: r.data.mensaje.alerta, tiempo: 3000}) 
            }else{
              minix({icon: 'success', mensaje: 'GUARDADO CON EXITO', tiempo: 3000}) 
            }

            return r.data
          }

        }else{
          minix({icon: 'info', mensaje: 'HUBO UN ERROR AL GUARDAR', tiempo: 6000})
        }

      } catch (error) {
        if (error.response.data.error.message) {
          minix({icon: 'error', mensaje: error.response.data.error.message, tiempo: 6000})
        }else{
          minix({icon: 'error', mensaje: error.message, tiempo: 3000})
        }
        console.warn(error)
      }
    },

    async obtener_data({commit, state, dispatch}, data){
      try {
        const config = {
          method: 'get',
          url: `${state.preferencias.IP}/api/${data.api}`,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${state.token_sesion}`
          }
        }

        let r = await axios(config)
        
        if (r.status == 200) {

          if (r.data.data.length == 0) {
            
            minix({icon: 'info', mensaje: 'NO HAY REGISTROS', tiempo: 3000})
            return []

          }else{
            return r.data.data
          }

        }else{
          return []
        }

      } catch (error) {

        console.log(error)
        
      }
    },

  },
  modules: {
  }
})
